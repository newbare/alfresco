/*
 * Copyright (C) 2005-2012 Alfresco Software Limited.
 *
 * This file is part of Alfresco
 *
 * Alfresco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Alfresco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Alfresco. If not, see <http://www.gnu.org/licenses/>.
 */
package org.alfresco.solr.query;

import java.io.IOException;

import org.apache.lucene.index.AtomicReader;
import org.apache.lucene.index.AtomicReaderContext;
import org.apache.lucene.search.Weight;
import org.apache.lucene.util.FixedBitSet;
import org.apache.solr.search.BitDocSet;
import org.apache.solr.search.DocSet;
import org.apache.solr.search.SolrIndexSearcher;

public class SolrDenySetScorer extends AbstractSolrCachingScorer
{

    SolrDenySetScorer(Weight weight, DocSet in, AtomicReaderContext context, SolrIndexSearcher searcher)
    {
        super(weight, in, context, searcher);
    }

    public static SolrDenySetScorer createDenySetScorer(Weight weight, AtomicReaderContext context, SolrIndexSearcher searcher, String authorities, AtomicReader reader) throws IOException
    {
        String[] auths = authorities.substring(1).split(authorities.substring(0, 1));
        
        DocSet deniedDocSet = new BitDocSet(new FixedBitSet(searcher.maxDoc()));
     
        for (String auth: auths)
        {
            DocSet docs = searcher.getDocSet(new SolrDeniedQuery(auth));
            // Add this authority's docs to the full set.
            deniedDocSet = deniedDocSet.union(docs);
        }
        
        // TODO: cache the full set? e.g. searcher.cacheInsert(CacheConstants.ALFRESCO_DENYSET_CACHE, authorities, deniedDocSet)
        // plus check of course, for presence in cache at start of method.
        return new SolrDenySetScorer(weight, deniedDocSet, context, searcher);
    }

}
